<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UpdateUsersCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to update User\'s name and timezone';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $faker = \Faker\Factory::create();

        for ($i=1; $i <= 20; $i++) {
            User::find($i)->update([
                'firstname' => $faker->firstname,
                'lastname' => $faker->lastname,
                'timezone' => $this->randomTZ[rand(1,9)] ?? $faker->timezone
            ]);
        }
    }
}
